/**
 * Component for MainMenu Feature
*/

import React, { PureComponent } from 'react';
import {
  Text,
  TouchableOpacity,
  View,
  FlatList
} from 'react-native';

class MainMenu extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
        selectedMainMenuId: this.props.mainMenu.length === 0  ? '' : this.props.mainMenu[0].id,
    }
  }

  selectMainMenu = (item) => {
      this.props.selectedMainMenu(item.categories)
      this.setState({
        selectedMainMenuId : item.id
    })
  }

  setSelectedMainMenuColor = (itemId) => {
    if(this.state.selectedMainMenuId === itemId) {
        return '#FFFFFF'
    } else {
        return 'black'
    }
  }

  renderMainMenu = (item) => {
    if (item !== null) {
        return (
          <TouchableOpacity
            style={{justifyContent: 'center'}}
            activeOpacity={1}
            onPress={() => {
              this.selectMainMenu(item)
            }}
          >
            <Text style={{margin: 10,fontSize: 22, color: this.setSelectedMainMenuColor(item.id) }}>{item.name}</Text>
          </TouchableOpacity>
        )
      } else {
        return null
      }
}
  
  render() {
    return (
        <View style={{flex: 1, paddingLeft: 22}}>
        <FlatList
          horizontal={true}
          data={this.props.mainMenu}
          renderItem={({item}) => this.renderMainMenu(item)}
        />
        </View>
    );
  }
}


export default MainMenu;
