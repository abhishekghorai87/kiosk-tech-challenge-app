/**
 * Component for ChieldMenu Feature
*/

import React, { PureComponent } from 'react';
import {
  Text,
  TouchableOpacity,
  View,
  FlatList
} from 'react-native';

class ChieldMenu extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      selectedChieldMenuId: this.props.chieldMenu.length === 0  ? '' : this.props.chieldMenu[0].id,
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.chieldMenu != prevProps.chieldMenu) {
        this.setState({
            selectedChieldMenuId: this.props.chieldMenu.length === 0  ? '' : this.props.chieldMenu[0].id,
        })
    }
  }

  selectChieldMenu = (item) => {
    this.props.selectedChieldMenu(item)
    this.setState({
        selectedChieldMenuId : item.id
    })
}

renderSelectedBorder = (itemId) => {
    if(this.state.selectedChieldMenuId === itemId) {
        return (
            <View style={{height: 10, margin: 10, backgroundColor: '#FFFFFF'}}></View>
        )
    }
}

setSelectedChieldMenuColor = (itemId) => {
    if(this.state.selectedChieldMenuId === itemId) {
        return '#FFFFFF'
    } else {
        return 'black'
    }
}

  renderChieldMenu = (item) => {
    if (item !== null) {
        return (
          <View>
            <TouchableOpacity
                style={{justifyContent: 'center'}}
                activeOpacity={1}
                onPress={() => {
                this.selectChieldMenu(item)
                }}
            >
                <Text style={{ margin: 10,fontSize: 20, color: this.setSelectedChieldMenuColor(item.id) }}>{item.name}</Text>
            </TouchableOpacity>
            {this.renderSelectedBorder(item.id)}
          </View>
        )
      } else {
        return null
      }
}
  
  render() {
    return (
        <View style={{flex: 1, paddingLeft: 22}}>
        <FlatList
          horizontal={true}
          data={this.props.chieldMenu}
          renderItem={({item}) => this.renderChieldMenu(item)}
        />
        </View>
    );
  }
}


export default ChieldMenu;
