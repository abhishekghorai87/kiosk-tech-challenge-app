/**
 * Component for ItemDetails Feature
*/

import React, { PureComponent } from 'react';
import {
  Image,
  Text,
  View,
  FlatList
} from 'react-native';

class ItemDetails extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      
    }
  }
  
  renderItem = (item) => {
      return(
          <View style={{margin: 20, flexDirection: 'row', height: 200, width: 315, backgroundColor : '#ffffff', borderRadius: 20}}>
              <View style={{flex: 0.6, flexDirection: 'column', marginLeft: 20}}>
                <View style={{flex: 0.7}}>
                    <Text style={{marginTop: 10,fontSize: 18, color: 'black', fontWeight: 'bold' }}  numberOfLines={2} ellipsizeMode='tail' >{item.name}</Text>
                    <Text style={{marginTop: 10,fontSize: 12, color: 'gray' }} numberOfLines={2} ellipsizeMode='tail' >{item.description}</Text>
                </View>
                <View style={{flex: 0.3, flexDirection: 'row', backgroundColor: '#ffffff'}}>
                    <View style={{flex: 0.5, backgroundColor: '#ffffff'}}>
                        <Text style={{fontSize: 18, color: 'black', fontWeight: 'bold' }}>${item.price}</Text>
                    </View>
                    <View style={{flex: 0.5, backgroundColor: '#ffffff'}}>
                        <Text style={{marginLeft: 10,fontSize: 18, color: 'gray' }}>Cal</Text>
                    </View>
                </View>
              </View>
              <View style={{flex: 0.4, backgroundColor: '#FFFFFF', borderBottomEndRadius: 20, borderTopEndRadius: 20}}>
                <Image
                    style={{width: '100%', height: 200, resizeMode: 'stretch', borderBottomEndRadius: 20, borderTopEndRadius: 20}}
                    source={{uri: 'https://via.placeholder.com/150'}}
                />
              </View>
          </View>
      )
  }
  
  render() {
    return (
        <View style={{flex: 1, }}>
            <Text style={{marginTop: 20, marginLeft: 20 ,fontSize: 32, color: 'black', fontWeight: 'bold' }} >{this.props.itemDetails.name}</Text>
        <FlatList
        numColumns={2}
          data={this.props.itemDetails.items}
          renderItem={({item}) => this.renderItem(item)}
        />
        </View>
    );
  }
}


export default ItemDetails;
