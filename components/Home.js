/**
 * Component for Home Feature
*/

import React, { PureComponent } from 'react';
import {
  Text,
  View,
} from 'react-native';
import homeStyles from '../styleSheet/homeStyles';
import MainMenu from './MainMenu';
import ChieldMenu from './ChieldMenu';
import ItemDetails from './ItemDetails';
import { windowHeight } from '../common/Utility'

class Home extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
        mainMenu : [],
        chieldMenu : [],
        itemDetails : [],
    }
  }

  // this Api call should be sepearted later in HelperApiServer.js class 
  resturantMenuListGetApi = () => {
    return fetch('http://localhost:3001/api/menu')
    .then((response) => response.json())
    .then((responseJson) => {

      this.setState({
          mainMenu: responseJson.data.MenuGroups,
          chieldMenu : responseJson.data.MenuGroups[0].categories,
          itemDetails : responseJson.data.MenuGroups[0].categories[0]
      }, function(){

      });

    })
    .catch((error) =>{
      console.log(error);
    });
  }
  
  componentDidMount(){
    this.resturantMenuListGetApi()
  }

  changeMainMenu = (categories) => {
      this.setState({
          chieldMenu: categories,
          itemDetails: categories[0]
      })
  }

  changeChieldMenu = (item) => {
    this.setState({
        itemDetails: item
    })
}

render() {
    return (
      <View style={{
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
      }}>
        <View style={{height: 60, backgroundColor: '#6a5acd'}}>
             <MainMenu mainMenu={this.state.mainMenu} selectedMainMenu={this.changeMainMenu}/>
        </View>
        <View style={{height: 60, flexDirection: 'row', backgroundColor: '#5B1689'}}>
            <View style={{flex: 1, height: 60, backgroundColor: '#5B1689'}}>
                <ChieldMenu chieldMenu={this.state.chieldMenu} selectedChieldMenu={this.changeChieldMenu}/>
            </View>           
        </View>
        <View style={{height: windowHeight() - 120, flexDirection: 'row', backgroundColor: '#C9B5D7'}}>
            <View style={{flex: 0.7, backgroundColor: '#C9B5D7'}}>
                <ItemDetails itemDetails={this.state.itemDetails}/>
            </View>
            <View style={{flex: 0.3, backgroundColor: '#C9B5D7'}}>
                <View style={{position: 'absolute', top: -15, left: 10, height: 200, width: 270, backgroundColor : '#FFFFFF', borderRadius: 20,}}>
                    <View style={{flex: 0.5, backgroundColor: '#FFFFFF', borderRadius: 20}}>
                        <Text style={{marginTop: 20, marginLeft: 20 ,fontSize: 32, color: 'black', fontWeight: 'bold' }} >Your Order</Text>
                        <Text style={{marginTop: 10, marginLeft: 20 ,fontSize: 18, color: 'gray'}} >Not Yours? Start over</Text>
                    </View>
                    <View style={{height: 1, backgroundColor: 'gray'}}></View>
                    <View style={{flex: 0.5, alignItems: 'center', justifyContent: 'center', backgroundColor: '#FFFFFF', borderRadius: 20}}>
                        <Text style={{marginLeft: 10,fontSize: 18, color: 'gray' }}>Add items to your order & they will show up here.</Text>
                    </View>
                </View>
            </View>
        </View>
      </View>
    );
  }
}


export default Home;
