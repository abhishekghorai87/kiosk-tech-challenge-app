/**
 * 
 * All styles related to Home component
 */

import { StyleSheet } from "react-native";

export default StyleSheet.create({
  homeTopContainerStyle: {
    //flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    flexDirection: 'column',
    //justifyContent: 'center',
    backgroundColor: "gray",
  },
  mainMenuContainerStyle: {
    flex: .1,
    //alignItems: "center",
    //justifyContent: "center",
    backgroundColor: "red",
    //width: '100%'
  },
  chieldMenuContainerStyle: {
    flex: .1,
    width: '100%',
    //alignItems: "center",
    //justifyContent: "center",
    backgroundColor: "green",
  },
  itemDetailsContainerStyle: {
    flex: .8,
    //alignItems: "center",
    //justifyContent: "center",
    backgroundColor: "blue",
  },
});
