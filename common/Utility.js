/**
 * All the utility methods will here,
*/

import { Dimensions } from 'react-native'

export function windowHeight() {
    let size = Dimensions.get('window').height;
    return size;
  }
  
  export function windowWidth() {
    let size = Dimensions.get('window').width;
    return size;
  }
  
  